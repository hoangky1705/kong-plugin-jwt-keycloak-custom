FROM kong:1.1.2 as builder

ENV LUAROCKS_MODULE=kong-plugin-jwt-keycloak-custom

RUN apk add --no-cache git zip && \
    git config --global url.https://github.com/.insteadOf git://github.com/ && \
    luarocks install ${LUAROCKS_MODULE} && \
    luarocks pack ${LUAROCKS_MODULE}

FROM kong:1.1.2

ENV KONG_PLUGINS="bundled,jwt-keycloak-custom"

COPY --from=builder ${LUAROCKS_MODULE}* /tmp/
RUN luarocks install /tmp/${LUAROCKS_MODULE}* && \
    rm /tmp/*