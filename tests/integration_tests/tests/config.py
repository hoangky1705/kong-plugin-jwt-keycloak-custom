import os

import requests

CLIENT_ID = os.environ.get("CLIENT_ID", "login")
CLIENT_SECRET = os.environ.get("CLIENT_SECRET", "ODPQ4dn6xjP40YxNohQyFgHLHV0I3gbW")

KONG_API = os.environ.get("KONG_API", "https://kong.toprate.io")
KONG_ADMIN = os.environ.get("KONG_ADMIN", "https://konga.toprate.io")

KC_USER = os.environ.get("KC_USER", "admin")
KC_PASS = os.environ.get("KC_PASS", "Pa55w0rd")
KC_HOST = os.environ.get("KC_HOST", "https://iam.toprate.io/auth")
KC_REALM = KC_HOST + "/realms/Thadico"

r = requests.post(KC_REALM + "/protocol/openid-connect/token", data={
    'grant_type': 'password',
    'client_id': 'login',
    'username': KC_USER,
    'password': KC_PASS
})

assert r.status_code == 200
KC_ADMIN_TOKEN = r.json()['access_token']

r = requests.get(KC_HOST + '/admin/serverinfo', headers={'Authorization': 'Bearer ' + KC_ADMIN_TOKEN})
assert r.status_code == 200
KC_VERSION = r.json()['systemInfo']['version']
